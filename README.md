platform-bionic: AOSP platform/bionic mod
=========================================

If you want to see original README please click [this link](README_orig.md).

## NOTICE

This project is part of [Finch](https://gitlab.com/fsi.mob1/finch); please
see earlier link for entire project description.

We may missed some requirements such as mentioning correct authors of our
dependent library, and so on. We never tries to violate other's, so if you
found mistake please feel free to contact us and we will gladly correct them.

## AUTHORS

Original works from https://android.googlesource.com/platform/bionic .

Modification tracked in this repository are made by belowing authors:

- SungHyoun Song <decash@fsec.or.kr>

## LICENSE

Apache 2.0 (Same as origin). See [LICENSE](LICENSE) for details.

You can see modification by diffing branch `master` and other named branch.
